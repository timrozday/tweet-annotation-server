FROM nginx:latest
LABEL Author="Tim Rozday" Email="timrozday@ebi.ac.uk"
LABEL Description="Webserver for clinical trials tweet annotations" Date="10Jun2020"


# Set up environmental vairables
ENV FLASK_APP /home/enduser/webservice/flask-server.py


# Install dependencies

RUN apt-get -y update && \
    apt-get -y install python3 python3-pip vim
RUN pip3 install flask sqlalchemy requests gunicorn pymysql


# Copy code across
ADD ./webservice /home/enduser/webservice
RUN chmod a+rwx /home/enduser/webservice/run.sh


# Configure nginx


# Set to non-root user

RUN useradd -ms /bin/bash enduser
USER enduser


# Run flask
EXPOSE 5000
EXPOSE 80
WORKDIR /home/enduser/webservice
CMD ["./run.sh"]