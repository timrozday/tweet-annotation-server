if [ -v $CREATE_DB ] || [ -v $IMPORT_DATA ]; then
    python3 create_db.py
fi
gunicorn -b :5000 -w 5 --access-logfile - --error-logfile - flask-server:app