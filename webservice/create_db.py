from utils import *
import os
import datetime

IMPORT_DIR = os.getenv('IMPORT_DIR')

DB_TYPE = os.getenv('DB_TYPE')
DB_URL = os.getenv('DB_URL')
DB_PORT = os.getenv('DB_PORT')
DB_NAME = os.getenv('DB_NAME')
DB_USERNAME = os.getenv('DB_USERNAME')
DB_PASSWORD = os.getenv('DB_PASSWORD')

db_manager = TweetDatabaseHandler(db_path=f"{DB_TYPE}://{DB_USERNAME}:{DB_PASSWORD}@{DB_URL}:{DB_PORT}/{DB_NAME}")

db_manager.create()

with open(f"{IMPORT_DIR}/tweets_data.pkl", 'rb') as f: 
    tweets_data = pickle.load(f)
with open(f"{IMPORT_DIR}/tweets_termite_data.pkl", 'rb') as f: 
    tweets_termite_data = pickle.load(f)
with open(f"{IMPORT_DIR}/complete_association_dict.pkl", 'rb') as f: 
    complete_association_dict = pickle.load(f)

associated_tweets = set()
for nct_id,tweets in complete_association_dict.items():
    associated_tweets.update({tweets_data[k]['tweet_id'] for k in tweets.keys()})

tweet_rows = []
for k,v in tweets_data.items():
    if isinstance(v['date'], str):
        v['date'] = datetime.datetime.strptime(v['date'], db_manager.date_format)

    tweet_termite_annotations = []
    if k in tweets_termite_data:
        for tweets_termite in tweets_termite_data[k]:
            tweet_termite_annotations.append(
                db_manager.Tweet_Termite_Annotation(
                    entity_type = tweets_termite['entity_type'],
                    entity_id = tweets_termite['entity_id'],
                    entity_name = tweets_termite['entity_name'],
                    match_syn = tweets_termite['match_syn'],
                    match_pos = tweets_termite['match_pos'],
                    score = tweets_termite['score'],
                    nonambig = tweets_termite['nonambig'],
                    meshtree = tweets_termite['meshtree'] if not tweets_termite['meshtree'] == 'NA' else None
                )
            )

    annotation_priority = 10 if v['tweet_id'] in associated_tweets else 0

    tweet_rows.append(
        db_manager.Tweet(
            twitter_id=v['tweet_id'], 
            user_name=v['user_name'], 
            text=v['text'], 
            url=v['url'], 
            date=v['date'], 
            annotation_priority=annotation_priority,
            tweet_termite_annotations=tweet_termite_annotations
        ) 
    )

session = db_manager.SessionMaker()
session.add_all(tweet_rows)
session.commit()
session.close()
