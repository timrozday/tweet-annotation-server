from flask import Flask, Blueprint
from utils import *
import os


DB_TYPE = os.getenv('DB_TYPE')
DB_URL = os.getenv('DB_URL')
DB_PORT = os.getenv('DB_PORT')
DB_NAME = os.getenv('DB_NAME')
DB_USERNAME = os.getenv('DB_USERNAME')
DB_PASSWORD = os.getenv('DB_PASSWORD')
db_manager = TweetDatabaseHandler(db_path=f"{DB_TYPE}://{DB_USERNAME}:{DB_PASSWORD}@{DB_URL}:{DB_PORT}/{DB_NAME}")

HYPOTHESIS_URL = os.getenv('HYPOTHESIS_URL')
HYPOTHESIS_GROUP = os.getenv('HYPOTHESIS_GROUP')
HYPOTHESIS_TOKEN = os.getenv('HYPOTHESIS_TOKEN')
hypothesis_manager = TweetHypothesisHandler(group=HYPOTHESIS_GROUP, token=HYPOTHESIS_TOKEN, hypothesis_api_url=HYPOTHESIS_URL)


app = Flask(__name__)

URL_PREFIX = os.getenv('URL_PREFIX')
bp = Blueprint('main', __name__, template_folder='templates', static_folder='static', url_prefix=URL_PREFIX)

from app import routes

app.register_blueprint(bp, url_prefix=URL_PREFIX)