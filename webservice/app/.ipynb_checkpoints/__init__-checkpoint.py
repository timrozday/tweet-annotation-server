from flask import Flask
from utils import *
import os

DB_TYPE = os.getenv('DB_TYPE')
DB_URL = os.getenv('DB_URL')
DB_PORT = os.getenv('DB_PORT')
DB_NAME = os.getenv('DB_NAME')
DB_USERNAME = os.getenv('DB_USERNAME')
DB_PASSWORD = os.getenv('DB_PASSWORD')

app = Flask(__name__)

# get environmental variables
mysql+pymysql


db_manager = TweetDatabaseHandler(db_path=f"{DB_TYPE}://{DB_USERNAME}:{DB_PASSWORD}@{DB_URL}:{DB_PORT}/{DB_NAME}")
hypothesis_manager = TweetHypothesisHandler()

from app import routes