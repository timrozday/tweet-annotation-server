$(document).ready(function(){
    var table = $('#records-table').DataTable();
    $.get('/list-records-data', function(data){
        table.rows.add(data.records_data).draw();
    });
});