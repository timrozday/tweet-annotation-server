$(document).ready(function(){
    $("#update-annotations .link").click(function(){
        $("#update-annotations .response").html("<img src='/static/img/loading.gif' width='140px'>")
        $.get({url: "/update-record", success: function(result){
            var response_html = "";
            for (var k in result){
                response_html = response_html + `<div class='row'><span class='key'>${k}</span>: <span>${result[k]}</span></div>`;
            }
            $("#update-annotations .response").html(response_html);
        }});
    });
});