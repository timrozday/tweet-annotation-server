$(document).ready(function(){
    $("#update-annotations .link").click(function(){
        $("#update-annotations .response").html("");
        $("#update-annotations .loading").show();
        $.get({url: `${url_prefix}/update-record`, success: function(result){
            var response_html = "";
            for (var k in result){
                response_html = response_html + `<div class='row'><span class='key'>${k}</span>: <span>${result[k]}</span></div>`;
            }
            $("#update-annotations .loading").hide();
            $("#update-annotations .response").html(response_html);
        }});
    });
});